import json
import re
import numpy as np

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import HashingVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn import svm
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory

class preprocess:
	def __init__(self, topic=None):
		self.vectorizer = CountVectorizer(min_df=1)
		self.documents = [] #  Document Kotor
		self.corpus = {} # Dokumen Bersi
		self.term_total = {}
		self.term_documents = {}
		self.total_document = 0
		self.topic = topic
		self.root_word = []
		self.kata_non_baku = {}
		self.tfidf = None
		self.stems = []
		self.json_data = []
		self.feature_name = None
		if self.topic is not None: self.topic = re.sub(r'(?:\.txt)','', self.topic)

	"""
	    Read File from txt
	    1 Document per Line ex 1 tweet, 1 video subtitle
	"""
	def add_Document(self, file):
		for line in file:
			self.documents.append(line)

	"""
	    Split Document into word
	    Input ex : ['data', 'data', 'data']
	    Output Dict : E.x : {0 : ['term','foo','baz'],1 : ['baz','foo','bar']}
	"""
	def tokenizer(self, token_data):
		tokenize = {}
		for list_words, i in zip(token_data, range(0, len(token_data))):
			# 1 tweet/1 video subtitle = 1 Document
			tokenize[i] = re.split(' ', list_words)
		return tokenize

	"""
	    Stemmer : pengakaran kata menggunakan library Sastrawi
		NB : need tweak to increase performance
	"""
	def stemmer(self, documents):
		stemming = []
		for document in documents:
			factory = StemmerFactory()
			stemmer_machine = factory.create_stemmer()
			stemming.append(stemmer_machine.stem(document))
		return stemming

	"""
	    Stopword Removal
	"""
	def non_root_removal(self,  corpus_dict):
		clean_words = []
		unclean_words = {}
		stop_words = []

		# kata dasar bahasa Indonesia
		kamus_data = '../Data/kata_dasar.txt'
		kamus_kata_dasar = open(kamus_data,"r")
		ina_kamus_data = []
		lines = kamus_kata_dasar.readlines()
		for line in lines:
			line = line.strip('\n')
			ina_kamus_data.append(line)

		# removal
		counter = 0
		pemisah = " "
		for document in corpus_dict.values():
			kata_dasar = []
			non_baku = []
			for word in document:
				for stopword in ina_kamus_data:
					if word == stopword:
						kata_dasar.append(word)
					else:
						non_baku.append(word)
			unclean_words[counter] = non_baku
			clean_words.append(pemisah.join(kata_dasar))
			counter += 1

		return clean_words

	"""
	    Stopword Removal
	"""
	def stopword_removal(self, corpus_dict):
		clean_words = []
		dictionary = {}

		pemisah = (' ')
		print len(corpus_dict)

		# open file
		kamus_data = '../Data/kamus_ivanlanin.txt'
		words = open(kamus_data,"r")

		# kata dasar bahasa Indonesia
		for word in words:
			#explode row, 1st is class, 2nd is lemma
			attribute = word.lower().split('\t')
			if len(attribute) != 2:
				key = attribute[0].replace('', '') #removing spaces if any
				key = key.rstrip('\n')
				#set to dictionary
				dictionary[key] = {'lemma' : key, 'class': None}
			else:
				key = attribute[0].replace('', '') #removing spaces if any
				#set to dictionary
				dictionary[key] = {'lemma': attribute[0], 'class': attribute[1].rstrip('\n')}

		# for key in dictionary.values():
		# 	if key['lemma'] == 'ini':
		# 		print key['class']

		# removal
		for term_list, x in zip(corpus_dict.itervalues(), range(0, len(corpus_dict))):
			helper = []
			for term in term_list:
				for key in dictionary.values():
					if key['lemma'] == term:
						if key['class'] == 'pron':
							pass
						elif key['class'] == 'n':
							pass
						elif key['class'] == 'p':
							pass
						elif key['class'] == None:
							pass
						elif key['class'] == 'adv':
							pass
						elif key['class'] == 'num':
							pass
						else :
							helper.append(term)
							pass
			clean_words.append(pemisah.join(helper))
		return clean_words

	def term_kamus_counts(self,__corpus__dict):
		# corpus insidental
		kamus_kait= '../Data/kamus_kait.txt'
		kamus_kata_kait = open(kamus_kait,"r")
		ina_kamus_kait = []
		lines = kamus_kata_kait.readlines()
		for line in lines:
			line = line.strip('\n')
			ina_kamus_kait.append(line)

		__kata_kait = []
		counter = 0
		pemisah = " "
		for document in __corpus__dict.values():
			kata_dasar = {}
			for word in document:
				for stopword in ina_kamus_kait:
					kata_dasar[stopword] = document.count(stopword)
			__kata_kait.append(kata_dasar)
		return __kata_kait

	def get_feature_names(self):
		return self.feature_name

	def documents_term_counts(self,  __corpus__dict):
		# corpus insidental
		kamus_kait= '../Data/kamus_kait.txt'
		kamus_kata_kait = open(kamus_kait,"r")
		ina_kamus_kait = []
		lines = kamus_kata_kait.readlines()
		for line in lines:
			line = line.strip('\n')
			ina_kamus_kait.append(line)

		__kata_kait = []
		counter = 0
		pemisah = " "
		for document in __corpus__dict.values():
			kata_dasar = {}
			for word in document:
				for stopword in ina_kamus_kait:
					kata_dasar[stopword] = document.count(stopword)
			__kata_kait.append(kata_dasar)
		self.feature_name = sorted(__kata_kait[1])

		counter = 0
		transform_tfidf = []
		for document in __kata_kait:
			helper = []
			for name in self.feature_name:
				helper.append(document[name])
			transform_tfidf.append(helper)
		return transform_tfidf

	def document_term_matrix(self, fit_transforms):
		transformer = TfidfTransformer()
		tfidf = transformer.fit_transform(fit_transforms)
		return tfidf.toarray()

	def word_sentimen(self,tfidf):
		"""
		Experimental
		"""
		text_data = "../Data/kamus_sentimen.txt"
		open_file = open(text_data, "r")
		lines = open_file.readlines()
		kamus_sentimen = {}
		for line in lines:
			line = line.strip('\n')
			line = line.lower().split('\t')

			kamus_sentimen[line[0]] = float(line[1])

		"""=============================="""
		sentimen_value = []
		for counter in tfidf:
			array = []
			for value,vocab in zip(counter, self.feature_name) :
				for sentimen in kamus_sentimen:
					if vocab == sentimen:
						value = value * kamus_sentimen[vocab]
				array.append(value)
			sentimen_value.append(array)
		return sentimen_value

	# def support_machine(self, learn_sentimen, Y, data):

	# 	clf = svm.SVC()
	# 	clf.fit(learn_sentimen, Y)

	# 	proses = preprocess()
	# 	open_file = open(data, "r")
	# 	proses.add_Document(open_file)
	# 	stems = proses.stemmer(proses.documents)
	# 	token = proses.tokenizer(stems)
	# 	removal = proses.stopword_removal(token)
	# 	tokens = proses.tokenizer(removal)


	# 	fit_transform = proses.documents_term_counts(tokens)
	# 	tfidf = proses.document_term_matrix(fit_transform)
	# 	tfidf_sentimen = proses.word_sentimen(tfidf)

	# 	# kamus = proses.term_kamus_counts(tokens)
	# 	# print kamus

	# 	csv_data = proses.term_counts_csv(tokens)
	# 	print csv_data
	# 	tfidf_print = proses.tfidf_data(tfidf, proses.feature_name)
	# 	write = writer.writer()

	# 	# write.csv_writer('Data Persen ', '../Data/',tfidf_print )
	# 	write.csv_writer('Data Frekuensi Kata','../Data/',csv_data)
	# 	#
	# 	# json_data = proses.term_counts_json(token)
	# 	# json_words_cloud = write.words_cloud(json_data)
	# 	# write.json_write('word_cloud_islammoderat_clean_1', json_words_cloud)

	def tfidf_data(self, tfidf, feature_name):
		test = []
		for tes in tfidf:
			tsp = []
			for v, x in zip(feature_name, range(0, len(tes))):
				tsp.append(v)
				tsp.append(tes[x])
			test.append(tsp)
		return test

	def term_counts_csv(self, corpus):
		data = {}
		data_persen = []
		term_csv = []

		total_document = float(0)
		for document in corpus.values():
			total_document += len(document)

		for document in corpus.values():
			for term in document:
				data[term] = data.get(term, 0) + 1
				counter = 0
				frequens = []
				if term in document:
					counter += 1
					frequens.append(term)
					frequens.append(counter)
					term_csv.append(frequens)

		term_persen = []
		for word in data:
			list_ = []
			persen = float(data[word]) / total_document * float(100)
			list_.append(word)
			list_.append(data[word])
			# list_.append(persen)
			term_persen.append(list_)

		return term_persen
		# return term_csv

	def term_counts_json(self, corpus):

		data = {}
		for document in corpus.values():
			for term in document:
				data[term] = data.get(term, 0.0) + 1.0
		return data

	"""
		type corpus : ['word','word_1']
	"""
	def  set_json_data(self,data,corpus):
		json_data = []
		for  counter in data:
			dict_2 ={}
			for value,vocab in zip(counter, self.get_feature_names()):
				dict_2[vocab] = value
			json_data.append(dict_2)
		return json_data
