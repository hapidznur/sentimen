import csv
import json
import csv
import re
class writer():
	"""docstring for writer"""
	def __init__(self):
		pass

	def csv_writer(self, filename, file_path, data):
		file = file_path+ filename + '.csv'
		with open(file, 'wb') as outfile:
			writer = csv.writer(outfile)
			writer.writerows(data)

	def json_write (self, filename, data):
		# filename = filename.split('/')
		# filename = re.sub(r'\.txt','',filename[2])
		savefile = '../Data/' + filename + ".json"
		try:
			jsondata = json.dumps(data)
			fd = open(savefile, 'w')
			fd.write(jsondata)
			fd.close()
		except:
			print 'ERROR writing', filename

	def words_cloud(self, data):
		cloud_data = []
		for  keys in data:
			word_size = {'text':keys,'size': data[keys] * 10}
			cloud_data.append(word_size)
		return cloud_data

	def words_cloud_csv(self, data):
		cloud_data = []
		for  keys in data:
			# print keys[1]
			word_size = {'sentimen':int(keys[2]), 'text':keys[0],'size': int(keys[1]) }
			cloud_data.append(word_size)
		return cloud_data

	def concat_text_file(self, path_file, filenames):
		# Examples
		# filenames = ['file1.txt', 'file2.txt', ...]
		with open(path_file, 'w') as outfile:
			for fname in filenames:
				with open(fname) as infile:
					outfile.write(infile.read())
