import json
import re
import numpy as np
import textmining
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import HashingVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn import svm

from Sastrawi.Stemmer.StemmerFactory import StemmerFactory

import stemming

class preprocess:
    def __init__(self, topic=None):
        self.documents = [] #  Document Kotor
        self.corpus = {} # Dokumen Bersi
        self.words_total = 0
        self.documents_term_count = {}
        self.documents_term = []
        self.term_total = {}
        self.term_documents = {}
        self.total_document = 0
        self.corpus_term = '../Data/kata_sentimen.txt'
        self.topic = topic
        self.root_word = []
        self.kata_non_baku = {}
        self.tfidf = None
        self.stems = []
        if self.topic is not None: self.topic = re.sub(r'(?:\.txt)','', self.topic)

    """
        Read File from txt
        1 Document per Line ex 1 tweet, 1 video subtitle
    """
    def add_Document(self, file):
        for line in file:
            self.documents.append(line)

    """
        Clean dokumen from Unecessery like punctuation or etc
        Input : List a document ex : ['lorem ipsum. Fernan ah id soget, mar to?', 'lorem opsum']
        Output : Dict with List a document ex : {0:['lorem ipsum fernan ah id soget mar to'], 1: ['lorem opsum']
    """
    def cleansing(self):
        list_words = self.documents
        for i in  range(0, len(list_words)):
            list_words[i] = re.sub(r'(?:\xe2\x80\x9c)','',list_words[i])
            list_words[i] = re.sub(r'(?:\xe2\x80\xa6)','',list_words[i])
            list_words[i] = re.sub(r'(?:\xe2\x80\x99)','',list_words[i])
            list_words[i] = re.sub(r'(?:\xe2\x80\x9c)','',list_words[i])
            list_words[i] = re.sub(r'(?:\xe2\x80\x98)','',list_words[i])
            list_words[i] = re.sub(r'(?:\xe2\x80\x93)','',list_words[i])
            list_words[i] = re.sub(r'(?:\xe2\x80\x9d)','',list_words[i])
            list_words[i] = re.sub(r'(?<=\w)\.(?=\w)',' ',list_words[i])
            list_words[i] = re.sub(r'(?:!|\?|\.{3}|\.|\.\D|\.\s|\,|[\n]|\(|\))','', list_words[i]) #punctuation
            list_words[i] = re.sub(r'(?:[A-Z]+[\s]+@[\w_]+[:]|\s[:])','',list_words[i]) # Retweet
            list_words[i] = re.sub(r'(?:@[\w_]+)','',list_words[i]) # @-mention
            list_words[i] = re.sub(r'(?:\#+[\w_]+[\w\'_\-]*[\w_]+[\w:]|\s[:])','',list_words[i]) # hash-tags
            list_words[i] = re.sub(r'<[^>]+>','',list_words[i]) #html tags
            list_words[i] = re.sub(r'http[s]?://(?:[a-z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-f][0-9a-f]))+','',list_words[i]) # URLs
            list_words[i] = list_words[i].lower()
            list_words[i] = re.sub(r'(?<=\w)  (?=\w)',' ', list_words[i])
            list_words[i] = re.sub(r'(?<=\w)\.\.(?=\w)',' ', list_words[i])
            list_words[i] = re.sub(r'(?<=\w)\,\.(?=\w)',' ', list_words[i])
            self.documents[i] = list_words[i]

    """
        Split Document into word
        Input ex : {0:['lorem ipsum etc'], 1: ['lorem opsum fernanda maha ah flanel loris']}
        Output Dict : E.x : {0 : ['term','foo','baz'],1 : ['baz','foo','bar']}
    """
    def tokenizing(self, stems):
        __corpus__dict = {}
        for list_words, i in zip(stems, range(0, len(stems))):
            # 1 tweet/1 video subtitle = 1 Document
            __corpus__dict[i] = re.split(' ', list_words)
        return __corpus__dict

    """
        Stopword Removal
    """
    def stopword_removal(self, __corpus__dict):
        __root_word = []
        __kata_non_baku = {}
        # Stopword bahasa Indonesia
        kamus_data = '../Data/kata_dasar.txt'
        kamus_kata_dasar = open(kamus_data,"r")
        data_bersih = []
        ina_kamus_data = []
        lines = kamus_kata_dasar.readlines()
        for line in lines:
            line = line.strip('\n')
            ina_kamus_data.append(line)

        # removal
        counter = 0
        pemisah = " "
        for document in corpus__dict:
            kata_dasar = []
            non_baku = []
            for word in document:
                for stopword in ina_kamus_data:
                    if word == stopword:
                        kata_dasar.append(word)
                    else:
                        non_baku.append(word)

            __kata_non_baku[counter] = non_baku
            __root_word.append(pemisah.join(kata_dasar))
            counter += 1
    return __root_word
        # print self.root_word
        # ina_stopword_path = '../Data/kamus_ivanlanin.txt'
        # stopword = open(ina_stopword_path,"r")
        # corpus_stopwords = []
        # text_stopwords = []
        # list_words = []
        # lines = stopword.readlines()
        # for line in lines:
        #     line = line.strip('\n')
        #     line = re.sub(r'\t', '', line)
        #     corpus_stopwords.append(line)
        #     # print line
        # # Menghilangkan stopword
        # remove = '|'.join(corpus_stopwords)
        # regex = re.compile(r'\b('+remove+r')\b')
        # for x in range(0,len(self.corpus)):
        #     for y in self.corpus[x]:
        #         pass
                # print type(self.corpus[y])
                # print type(self.corpus[x][y])
                # list_words[x][y] = regex.sub('', self.corpus[x][y])
                # list_words.append(list_words[x][y])

        # print list_words
        # double_word = []
        # seen = set()
        # for text_dict in list_words:
        #     # If value has not been encountered yet,
        #     # ... add it to both list and set.
        #     if text_dict not in seen:
        #         double_word.append(tweet_text)
        #         seen.add(tweet_text)
        # pass

    """
        Menghitung Munculnya suatu kata di Dokumen
        Jika 1 Line 1 Dokumen Maka total kata di seluruh dokumen
        Input ex : {0:['lorem ipsum etc'], 1: ['lorem opsum fernanda maha ah flanel loris']}
        Output : {'term': number_type_float} ex : {'lorem': 2.0 }
    """
    def term_counts(self, corpus):
        for document in corpus:
            for term in corpus[document]:
                self.term_total[term] = self.term_total.get(term, 0.) + 1.0
                self.words_total += len(corpus[document])
        return self.words_total

    """
        Stemming pengakaran kata baku
    """
    def stemming(self, __documents):
        __stems = []
        for document in __documents:
            factory = StemmerFactory()
            stemmer = factory.create_stemmer()
            __stems.append(stemmer.stem(document))
        return __stems


    """
    Menghitung Munculnya suatu kata di Dokumen dan total seluruh kata.
    Input ex : {0:['lorem ipsum etc'], 1: ['lorem opsum fernanda maha ah flanel loris']}
    Output : t{'document_number': {'term':number_type_float}} ex : { 0 :{'lorem': 2.0 }, 0 :{'lorem': 2.0 }}
    """
    def document_term(self):
        for document in self.corpus:
            list_word = {}
            for term in self.corpus[document]:
                    list_word[term] = self.corpus[document].count(term)
            self.term_documents[document] = list_word

    # """
    #     Jumlah Dokumen dengan kata tertentu
    # """
    def document_with_term(self):
        for __term in self.term_total:
            score = 0
            for document in self.corpus.itervalues():
                if __term in document:
                    score += 1
                    self.documents_term.append(__term)
                    self.documents_term_count[__term] = score

        def document_term_matrix(self, __root_word):
        vectorizer = CountVectorizer(min_df=1)
        # test = [['aku', 'makan', 'nasi'],['kamu', 'suka', 'bayem'],['doni', 'makan', 'nasi', 'dan', 'bayem']]
        # test1 = [['aku', 'makan', 'nasi'],['kamu', 'suka', 'bayem'],['doni', 'makan', 'nasi', 'dan', 'bayem']]
        # test2 = [['aku', 'makan', 'nasi'],['kamu', 'suka', 'bayem'],['doni', 'makan', 'nasi', 'dan', 'bayem']]
        X = vectorizer.fit_transform(__root_word)
        # print vectorizer.get_feature_names()
        word_counts = np.array(X.toarray())
        transformer = TfidfTransformer()
        tfidf = transformer.fit_transform(X.toarray())
        return tfidf = tfidf.toarray()

        def support(self):
        """
        Experimental
        """
        text_data = "../Data/kamus_sentimen.txt"
        open_file = open(text_data, "r")
        lines = open_file.readlines()
        kamus_sentimen = {}
        for line in lines:
            line = line.strip('\n')
            line = line.lower().split('\t')
            kamus_sentimen[line[0]] = float(line[1])
        """=============================="""

        sentimen_value = []
        for counter in self.tfidf:
            array = []
            for value,vocab in zip(counter, vectorizer.get_feature_names()) :
                for sentimen in  kamus_sentimen:
                    if vocab == sentimen:
                        value = value * kamus_sentimen[sentimen]
                array.append(value)
            sentimen_value.append(array)
        print np.array(sentimen_value)
        y = [1,-1,1]
        clf = svm.SVC()
        print clf.fit(sentimen_value, y)
        proses = preprocess.preprocess()
        proses.add_Document(open_file)
        proses.stemming()
        proses.tokenizing()
        proses.stopword_removal()

        proses.document_term_matrix()
        # proses.support()

        print clf.predict()
        print sentimen_value
