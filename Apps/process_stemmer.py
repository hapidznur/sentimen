"""
    python proses.py data.txt kamus_stopword.txt
"""

if __name__=='__main__':
    import preprocess
    import sys
    import file_open
    import types
    import writer

    mypath = '/home/placebo/Documents/Apps/Data/IslamRadikal/7/'
    # mypath = '/home/placebo/Documents/Apps/Data/example/'

    """
    	Read File
    """
    files = file_open.Open_file()
    corpus = files.file_folder(mypath)
    print len(corpus)
    """
	    Preproses seperti cleansing dan lain-lain
    """
    proses = preprocess.preprocess()
    stems = proses.stemmer(corpus)
    print stems
